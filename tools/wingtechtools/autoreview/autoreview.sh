﻿#!/bin/bash
#author=zwb

#首先获取me信息
me=$(git config --list|grep "user.name")
me=${me##*=}

#获取me所在用户组
group=$(cat android/tools/wingtechtools/autoreview/.group/group.con|grep "${me}=")
group=${group##*=}

#获取branch信息
if [ "$1" = "" ]; then
 echo "请输入要审核所对应的分支名"
 exit	
else	
 branch=$1
fi

if [ "$2" = "" ]; then
 echo "请输入提交人的名字"
 exit	
else 	
 owner=$2
fi


if [ "${group}" = "" ];then
  echo "ERROR:您没有自动审核的权限，请联系张维波！"
  exit
fi	

if [ "${group}" = "leader" ];then

  #再次需要提示风险
  echo "* 自动审核工具使用须知：               *"
  echo "* 1.只有每个组的leader可以做自动审核        *"
  echo "* 2.自动审核会把gerrit审核服务器中Incoming列表中指定分支的所有审核记录全部批量审核 *"
  echo "输入y进入自动审核："
	
  read -a start
  if [ "${start}" = "y" ]; then	
  ssh -p 29418 192.168.7.174 gerrit query status:open owner:${owner} reviewer:${me} branch:${branch} --current-patch-set|grep "revision" >tmp.txt
  perl android/tools/wingtechtools/autoreview/autoreview.pl tmp.txt
  rm tmp.txt
  fi
else
  echo "您没有自动审核的权限:"
  echo "输入y通知组长：${group}"
  
  read -a start
  if [ "${start}" = "y" ]; then	
     ssh -p 29418 192.168.7.174 gerrit query status:open owner:${owner} reviewer:${me} branch:${branch} --current-patch-set|grep "revision" >tmp.txt
     perl android/tools/wingtechtools/autoreview/addreviewer.pl tmp.txt ${group}
     rm tmp.txt
  fi
fi

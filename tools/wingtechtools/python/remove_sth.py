#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import commands
import sys

def wt_get(cmd):
    return commands.getstatusoutput(cmd)[1]
    
def usage():
    print "usage:  ./remove_sth.py \"start_string\" \"end_string\" file"
    print "start_string : string start flag which will be remove , not with space in it "
    print "end_string   : string end flag which will be remove , not with space in it "
    print "file  �� which file will be remove string"
    print "attention : start string flag and end string flag must be in pairs"
    sys.exit(-1)
    
    
if __name__ == '__main__':
    if not len(sys.argv) ==4:
        usage()
    start_string=sys.argv[1]
    end_string=sys.argv[2]
    parse_file=sys.argv[3]
    if not os.path.isfile(parse_file):
        print "error : file not exist :" + parse_file
        usage()
    
    start_line_no=wt_get('grep -n "%s" %s | cut -d":" -f1'%(start_string,parse_file)).split("\n")
    end_line_no=wt_get('grep -n "%s" %s | cut -d":" -f1'%(end_string,parse_file)).split("\n")
    if not len(start_line_no)==len(end_line_no):
        print "error :start string flag and end string flag must be in pairs"
        print "        start string counts(%s) !=   end string counts(%s) "%(len(start_line_no),len(end_line_no))
        sys.exit(-1)
    i=0
    cmd_tmp=""
    while i < len(start_line_no):
        if len(start_line_no[i]) > 0:
            print "%s-%s lines will be removed"%(start_line_no[i],end_line_no[i])
            cmd_tmp=cmd_tmp+start_line_no[i]+","+end_line_no[i] + "d;"
            if int(start_line_no[i]) > int(end_line_no[i]):
                print "error : start flag must before end flag"
                sys.exit(-1)
        i=i+1
    if len(cmd_tmp) > 0:
        if not commands.getstatusoutput("sed -i \"%s\" %s"%(cmd_tmp,parse_file))[0]==0:
            print "error : plz check , remove string failed"
            sys.exit(-1)

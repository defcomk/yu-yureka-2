#!/usr/bin/env python
import os
import sys
import time
import xlrd
import xlwt
import commands

def get_branch():
    branch_list = []
    cmd = "cd android/frameworks/base;git branch -a;cd -"
    out = commands.getstatusoutput(cmd)[1]
    for one in out.split():
        if one.startswith("remotes/aosp/"):
            branch_list.append(one[13:])
        if one.startswith("aosp/"):
            current_branch = one[5:]
    br = raw_input("Remote branch        [%s/?]: "%current_branch)
    if br == "":
        br=current_branch
    elif br == "?":
        print "The remote branch list as:"
        for one in branch_list:
            print "\t"+one
        return get_branch()
    else:
        if br not in branch_list:
            print "Sorry, try again!"
            return get_branch()
    return br
        
def get_time1():
    t1 = raw_input("Date from            [2014-08-01]: ")
    if t1 == "":
        t1 = "2014-08-01"
    elif t1 < "2014-08-01" or t1 > "9999-12-31":
        print "Sorry, try again!"
        return get_time1()
    return t1

def get_time2():
    t2 = raw_input("Date to              [9999-12-31]: ")
    if t2 == "":
        t2 = "9999-12-31"
    elif t2 < "2014-08-01" or t2 > "9999-12-31":
        print "Sorry, try again!"
        return get_time2()
    return t2

def get_keyword():
    w = raw_input("Key word             [?]: ")
    return w

def get_all_logs():
    mark = "|=====|"
    loga = '--pretty=format:"%H' + mark + '%cn' + mark + '%cd' + mark + '%s"'
    cmd  = "repo forall -pc git log remotes/aosp/%s --date=iso %s > .repo/gen_logs_tmpfile"%(branch,loga)
    os.system(cmd)

def generate():
    mark="|=====|"
    get_all_logs()
    f=open(".repo/gen_logs_tmpfile")
    lines=f.readlines()
    i=1
    wbk = xlwt.Workbook(encoding='utf-8')
    sheet = wbk.add_sheet('she9')
    sheet.write(0,0,'Git')
    sheet.write(0,1,'Commit ID')
    sheet.write(0,2,'Committer')
    sheet.write(0,3,'Time')
    sheet.write(0,4,'Log')
    for one in lines:
        if mark not in one:
            git_lib = one[:-1]
        else:
            one_time=one.split(mark)[2]
            if one_time >= begin_time and one_time <= end_time and keyword in one:
                sheet.write(i,0,git_lib[16:])
                sheet.write(i,1,one.split(mark)[0])
                sheet.write(i,2,one.split(mark)[1])
                sheet.write(i,3,one.split(mark)[2])
                sheet.write(i,4,one.split(mark)[3])
                i=i+1
    wbk.save('OUT.xls')
    print "\nGenerate Successed !"
    print "="*80
    
    

if __name__ == '__main__':
    print "*** Generate logs to Excel"
    print "***\n"
    print "*** Branch"
    print "***\n"
    branch  = get_branch()
    print "\n*** Time"
    print "***\n"
    begin_time = get_time1()
    end_time   = get_time2()
    print "\n*** Keyword Search"
    print "***\n"
    keyword = get_keyword()
    print "\n"
    print "="*80
    print "Now start to search and generate, waiting please ..."
    print "Branch:  "+branch
    print "From:    "+begin_time
    print "To:      "+end_time
    print "Keyword: "+keyword
    generate()




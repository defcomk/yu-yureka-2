#!/usr/bin/env python
import os
import sys
import time
import commands

def goback(time):
    root = commands.getstatusoutput("pwd")[1]
    f = open(".repo/project.list")
    lines = f.readlines()
    f.close()
    for one in lines:
        print "="*80
        print one
        one = one[:-1]
        os.chdir(root + "/" + one)
        os.system("git log --date=iso --pretty=format:'%H=====%cd'>tmplog")
        t = open("tmplog")
        logs = t.readlines()
        for temp in logs:
            a = temp.split("=====")[1]
            temp_time = a[:10]+"_"+a[11:19]
            if temp_time < time:
                os.system("git checkout %s"%temp.split("=====")[0])
                break
    os.chdir(root)
    print "Successed!"

if __name__ == '__main__':
    try:
        time = sys.argv[1]
        a = time.split("_")[0]
        b = time.split("_")[1]
    except Exception:
        print "For example:"
        print "./wt goback 2014-01-01_12:23:00"
        sys.exit(-1)
    goback(time)

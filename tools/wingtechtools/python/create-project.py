#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import sys
import time
import commands
import getopt

def mymkd(path):
    os.system("mkdir -p %s"%path)
    os.system("touch %s/.gitignore"%path)

newproject = sys.argv[1]

os.chdir("android")

mymkd("wingcust/%s/base"%newproject)
mymkd("wingcust/%s/base/device/overlay"%newproject)
mymkd("wingcust/%s/base/google"%newproject)
mymkd("wingcust/%s/base/manifest"%newproject)
mymkd("wingcust/%s/base/product"%newproject)
mymkd("wingcust/%s/base/wt_overlay"%newproject)
mymkd("wingcust/%s/base/checklist"%newproject)
files=os.listdir('tools/wingtechtools/checklists/')
for file in files:
    os.system("cp tools/wingtechtools/checklists/%s wingcust/%s/base/checklist/%s"%(file,newproject,file.replace('{��Ŀ��}',newproject)))
print "==================================\nSuccessed!"

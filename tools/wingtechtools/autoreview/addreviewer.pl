#!/usr/bin/perl
#author=zwb
use Cwd;
use Encode;
#use strict;

#print "addreviewer.pl::start...\n";

if(scalar(@ARGV) < 1){
	die "addreviewer.pl::parameter error";
}else
{
	$toreview_file=$ARGV[0];
	$reviewer=$ARGV[1];
}

open(TMP,"<$toreview_file") or die "addreviewer.pl::can't open file $toreview_file!";
my @lines=<TMP>;
close TMP;
my $reviewed = undef;

foreach my $line(@lines){

  if($line =~ /^\s*revision:\s([0-9a-f]+)\s*$/){

   my $commit = $1;
   $reviewed = "true";
   system("ssh -p 29418 192.168.7.174 gerrit set-reviewers $commit --add $reviewer");
   
  }
}
use encoding "utf-8",STDOUT=>"utf-8";
if($reviewed eq undef){
	print "没什么需要审核的节点。\n";
	exit;
}

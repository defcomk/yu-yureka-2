#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import sys
import time
import commands
import getopt
import getpass
from ftplib import FTP

NO_OUT = ">/dev/null 2>&1"
OTA_INFO = ["192.168.38.172","21","scm_xa","123456"]
OFTF = "build/tools/releasetools/ota_from_target_files"

ds1 = {
    "project":"",
    "customer":"",
    "maincarrier":"",
    "server_sign":'',
    "update-api":"",
    "make":False,
    "eng":False,
    "userdebug":False,
    "split":False,
    "ota":False,
    "otadiff":False,
    "package":False,
    "upload":False,
    "lenovo":False,
    "include-carrier":False,
    "carrier-all":False,
    "qmss":False,
    "releasenote":False,
    "record":False,
    "help":False,
    "wtid":"",
}

#usage
def usage():
    print "./wt publish: You must specify `-p' options"
    print "Try './wt publish -h' or './wt publish --help'for more information."
    sys.exit(-1)

def dohelp():
    print "="*80
    print "Usage: ./wt publish -p [project] [OPTION...]"
    print "    or ./wt publish -p [project] -c [customer] [OPTION...]"
    print "    or ./wt publish -p [project] -c [customer] -r [carrier] [OPTION...]"
    print "\nExamples:"
    print "   ./wt publish -p wt86518 -c A1 -mt           #Build wt86518_A1-eng"
    print "   ./wt publish -p wt86518 -c A1 -mko          #Build wt86518_A1-user and package otafull"
    print "\nMain operation mode:\n"
    print "  -h,  --help                read usage"
    print "  -p,  --project             project name,like wt86047. Necessarily"  
    print "  -c,  --customer            customer name,like A1,B1" 
    print "  -r,  --maincarrier         main carrier,like CM,CU,CT,CTA" 
    print "  -r,  --server_sign         sign ap img in server" 
    print "  -i,  --update-api          make update-api before make android"
    print "  -m,  --make                make android and fullbuild(has the function of -v)"
    print "  -t,  --eng                 build eng mode"
    print "  -s,  --userdebug           build userdebug mode"
    print "  -v,  --split               split imgs and repackage"
    print "  -k,  --package             pack files after build, like an on-off"
    print "  -o,  --ota                 pack otafull"
    print "  -a,  --otadiff             pack otadiff"
    print "  -q,  --qmss                pack qmss"
    print "  -n,  --releasenote         generate Release-note"
    print "  -e,  --record              record versin infomation"
    print "  -u,  --upload              upload ota and vmlinx and symbol to FTP"
    print "  -L,  --lenovo              LENOVO key "
    print "  -O,  --include-carrier     like LENOVO OVERSEA , if you want build all carriers , please use -A"
    print "  -A,  --carrier-all         build ALL carriers"

    print "\n"
    print "If you have any question,contact to zhouyanjiang@wingtech.com plz!"
    print "\n"
    sys.exit(-1)

#analysis parameter
def parse_options(argv,ds1):
    strings = "himsvtoanekuqLOAp:c:r:S"
    lists = ["help",
             "update-api",
             "make",
             "userdebug",
             "split",
             "eng",
             "ota",
             "otadiff",
             "releasenote",
             "record",
             "package",
             "upload",
             "qmss",
             "lenovo",
             "include-carrier",
             "carrier-all",
             "project=",
             "customer=",
             "maincarrier=",
             "wtid=",
             "server_sign=",]

    try:
        opts,args = getopt.getopt(argv, strings, lists)
    except getopt.GetoptError, err:
        usage()

    for o,a in opts:
        if o in ("-i", "--update-api"): 
            ds1["update-api"] = True
        elif o in ("-h", "--help"):
            ds1["help"] = True
        elif o in ("-m", "--make"):
            ds1["make"] = True
        elif o in ("-t", "--eng"):
            ds1["eng"] = True
        elif o in ("-s", "--userdebug"):
            ds1["userdebug"] = True
        elif o in ("-v", "--split"):
            ds1["split"] = True
        elif o in ("-o", "--ota"):
            ds1["ota"] = True
        elif o in ("-a", "--otadiff"):
            ds1["otadiff"] = True
        elif o in ("-n", "--releasenote"):
            ds1["releasenote"] = True
        elif o in ("-e", "--record"):
            ds1["record"] = True
        elif o in ("-k", "--package"):
            ds1["package"] = True
        elif o in ("-q", "--qmss"):
            ds1["qmss"] = True
        elif o in ("-L", "--lenovo"):
            ds1["lenovo"] = True
        elif o in ("-O", "--include-carrier"):
            ds1["include-carrier"] = True
        elif o in ("-A", "--carrier-all"):
            ds1["carrier-all"] = True
        elif o in ("-u", "--upload"):
            ds1["upload"] = True
        elif o in ("-p", "--project"): 
            ds1["project"] = a
        elif o in ("-c", "--customer"):
            ds1["customer"] = a
        elif o in ("-r", "--maincarrier"):
            ds1["maincarrier"] = a
        elif o in ("--wtid"):
            ds1["wtid"] = a
        elif o in ("-S", "--server_sign"):
            ds1["server_sign"] = 'true'

    if ds1["help"] == True:
        dohelp()
    if ds1["project"] == "":
        usage()
    if ds1["make"] == True:
        ds1["split"] = True
        ds1["update-api"] = True
    if ds1["carrier-all"] == True:
        ds1["include-carrier"] = True

#custom print:  add date and time 
#such as:
#===2014-01-01 20:45:12 LOG: Test information
def wt_print(mode,message):
    date  = time.strftime("%Y-%m-%d")
    time1 = time.strftime("%H:%M:%S")
    mark = "==="
    prelog = mark + date + " " + time1 + mark
    if   mode == "m": 
        print prelog + "LOG: " + message
    elif mode == "w": 
        print prelog + "WARNING: " + message
    elif mode == "e": 
        print prelog + "ERROR: " + message
        sys.exit(-1)

def wt_successed():
    wt_print("m","Successed!")

#exe command without the screem information
def wt_sys(cmd):
    os.system("%s %s"%(cmd,NO_OUT))

#get the screem information
def wt_get(cmd):
    return commands.getstatusoutput(cmd)[1]

#custom commond : mkdir
def wt_remkd(path):
    wt_sys("rm -rf %s"%path)
    wt_sys("mkdir -p %s"%path)

def wt_mkdir(path):
    if not os.path.exists(path):
        wt_sys("mkdir -p %s"%path)

def gen_lines(filename):
    f = open(filename)
    lines = f.readlines()
    f.close()
    return lines

def gen_ota_server():
    server = OTA_INFO[0]
    publishconfig = "/home/%s/.publishconfig"%getpass.getuser()
    if os.path.exists(publishconfig):
        for one in gen_lines(publishconfig):
            if "OTA_SERV" in one:
                server = one[:-1].split()[-1]
    return server

def gen_ota_port():
    port = OTA_INFO[1]
    publishconfig = "/home/%s/.publishconfig"%getpass.getuser()
    if os.path.exists(publishconfig):
        for one in gen_lines(publishconfig):
            if "OTA_PORT" in one:
                port = one[:-1].split()[-1]
    return port

def gen_ota_user():
    user = OTA_INFO[2]
    publishconfig = "/home/%s/.publishconfig"%getpass.getuser()
    if os.path.exists(publishconfig):
        for one in gen_lines(publishconfig):
            if "OTA_USER" in one:
                user = one[:-1].split()[-1]
    return user

def gen_ota_pass():
    password = OTA_INFO[3]
    publishconfig = "/home/%s/.publishconfig"%getpass.getuser()
    if os.path.exists(publishconfig):
        for one in gen_lines(publishconfig):
            if "OTA_PASS" in one:
                password = one[:-1].split()[-1]
    return password

# Todo:ftp operates
def wt_ftp(mode,strings):
    ftp = FTP()
    try:
        ftp.connect(OTA_SERV,OTA_PORT)
        ftp.login(OTA_USER,OTA_PASS)
    except Exception:
        wt_print("e","Connect to FTP failed!")
    if mode == "list":
        try:
            return ftp.nlst(strings)
        except Exception:
            return []
    if mode == "upload":
        ldir = strings.split(":")[0]
        rdir = strings.split(":")[1]
        if not os.path.exists(ldir):
            wt_print("e","Direction not found: %s"%ldir)
        else:
            localnames = os.listdir(ldir)
        try:
            ftp.mkd(rdir)
        except Exception:
            wt_print("m","Direction existed on FTP: %s"%rdir)
        for item in localnames:
            src = os.path.join(ldir,item)
            file_handler = open(src,'rb')
            ftp.storbinary("STOR %s"%os.path.join(rdir,item),file_handler)
            file_handler.close()
        ftp.quit()
    if mode == "download":
        rdir = strings.split(":")[0]
        ldir = strings.split(":")[1]
        wt_sys("mkdir -p %s"%ldir)
        try:
            rlist = ftp.nlst(rdir)
        except Exception:
            wt_print("e","Direction not found on FTP: %s"%rdir)
        for one in rlist:
            item = one.split(rdir)[1][1:]
            src = os.path.join(ldir,item)
            file_handler = open(src,'wb').write
            ftp.retrbinary("RETR %s"%one,file_handler)
        ftp.quit()

def check_otadiff():
    wt_print("m","Check environment")
    listr = wt_ftp("list","PUBLISH/%s_ota"%ver_inn)
    if "PUBLISH/%s_ota/old.zip"%ver_inn in listr:
        wt_print("e", "old.zip found in FTP: PUBLISH/%s_ota, plz remove it and rerun the script!"%ver_inn)
    old_ota_version_file = "android/wingcust/%s/%s/manifest/old_ota_versions"%(ds1["project"],custdir)
    if not os.path.exists(old_ota_version_file):
        if "_V001_" in ver_inn:
            ds1["otadiff"] = False
        else:
            wt_print("e","File not found: %s"%old_ota_version_file)
    message1 = "\nCurrent Version: %s\n"%ver_inn
    message2 = "Old versions as:"
    out = wt_get("cat %s"%old_ota_version_file).split()
    for one in out:
        message2 = message2 + "\n" + " "*17 + one
    message = message1 + message2
    print message+"\n"
    if ver_inn in message2:
        wt_print("e","The current version isn't the newest one, plz check it!")
    for one in out:
        rdir = one+"_ota"
        if not "PUBLISH/%s/old.zip"%rdir in wt_ftp("list","PUBLISH/"+rdir):
            wt_print("e","File is not found in %s: old.zip"%rdir)
    wt_successed()


def check_env():
    if ds1["otadiff"] == True:
        check_otadiff()
    if not os.path.exists("android/wingcust/Android.mk"):
        os.system("touch android/wingcust/Android.mk")

# generate the whole project name from project and customer
# if there is no customer,the whole name is project
# if there is a customer, the whole name is project with customer
def gen_project():
    if ds1["customer"] == "":
        project = ds1["project"]
    else:
        project = ds1["project"]+"_"+ds1["customer"]
    return project

# generate building mode from paras
def gen_mode():
    if ds1["eng"] == True:
        mode = "eng"
    elif ds1["userdebug"] == True:
        mode = "userdebug"
    else:
        mode = "user"
    return mode

# inner version
# grep the macro named WT_INNER_VERSION from ProjectConfig.mk
def gen_inn_version():
    filename = "android/wingcust/%s/%s/device/ProjectConfig.mk"%(ds1["project"],custdir)
    if not os.path.exists(filename):
        filename = "android/device/qcom/%s/ProjectConfig.mk"%project
    if not os.path.exists(filename):
        wt_print("e","WT_INNER_VERSION not found in ProjectConfig.mk!")
    inn = wt_get("grep -irn WT_INNER_VERSION %s"%filename).split("=")[-1]
    if inn[0] == " ":
        inn = inn[1:]
    return inn
    
def gen_tcl_custom_svn_version():
    filename = "android/wingcust/%s/%s/device/ProjectConfig.mk"%(ds1["project"],custdir)
    if not os.path.exists(filename):
        filename = "android/device/qcom/%s/ProjectConfig.mk"%project
    if not os.path.exists(filename):
        cus = project
    else:
        try:
            cus = wt_get("grep -irn WT_TCL_CUSTOMER_SVN %s"%filename).split("=")[-1].strip()
        except Exception:
            cus = ""
    return cus

def gen_ro_tct_sys_ver():
    filename = "android/wingcust/%s/%s/device/system.prop"%(ds1["project"],custdir)
    if not os.path.exists(filename):
        filename = "android/device/qcom/%s/system.prop"%project
    if not os.path.exists(filename):
        cus = project
    else:
        try:
            cus = wt_get("grep -irn \"ro.tct.sys.ver\" %s"%filename).split("=")[-1].strip()
        except Exception:
            cus = "0000000000"
    return cus[1:4]+cus[6:7]+cus[4:8]

# customer version
# grep the macro named WT_PRODUCTION_VERSION from ProjectConfig.mk
def gen_cus_version():
    filename = "android/wingcust/%s/%s/device/ProjectConfig.mk"%(ds1["project"],custdir)
    if not os.path.exists(filename):
        filename = "android/device/qcom/%s/ProjectConfig.mk"%project
    if not os.path.exists(filename):
        cus = project
    else:
        try:
            if project.startswith("wt82918tra"):
                cus = wt_get("grep -irn WT_BUILD_NUMBER %s"%filename).split("=")[-1]
            else:
                cus = wt_get("grep -irn WT_PRODUCTION_VERSION %s"%filename).split("=")[-1]
        except Exception:
            cus = project
        if cus[0] == " ":
            cus = cus[1:]
    return cus

def gen_remote_branch():
    os.chdir(root)
    lines = gen_lines(".repo/manifest.xml")
    branch_prj=""
    for one in lines:
        if project in one:
            branch_prj = one.split('"')[-2]
        if "<default revision=" in one:
            branch = one.split('"')[1]
    if branch_prj =="":
        return branch
    else:
        return branch_prj
    
#make command
#include update-api, make and fullbuild
def make():
    os.chdir("%s/android"%root)
    choice = project+"-"+mode
    if ds1["maincarrier"]  == "":ds1["maincarrier"] = "DEFAULT"

    cmd1 = "source ./build/envsetup.sh %s;"%NO_OUT
    cmd2 = "lunch %s %s;"%(choice,NO_OUT)
    cmd3 = "export WT_MAINCARRIER=%s;export WT_SIGN_SERVER=%s;export WTID=%s ;"%(ds1["maincarrier"],ds1["server_sign"],ds1["wtid"])
    cmd4 = "export WT_LENOVO_CARRIER_ALL=%s"%ds1["carrier-all"]
    cmd = cmd1 + cmd2 + cmd3 + cmd4

    log_path = "out/target/product/"
    wt_sys("mkdir -p %s"%log_path)

    if ds1["update-api"] == True:
        log1 = "%s/%s_update-api.log"%(log_path,project)
        wt_print("m","Begin to build update-api ...")
        os.system("%s;make -j`grep processor /proc/cpuinfo | wc -l` update-api 2>&1| tee -a %s"%(cmd,log1))
        f = open(log1)
        lines = f.readlines()
        f.close()
        if "Copying current.txt" in lines[-1] or "successfully" in lines[-2]:
            wt_print("m","Successed!")
        else:
            wt_print("e","Update-api ERROR! Check log @ %s"%log1)

    wt_print("m","Begin to build android ...")
    log2 = "%s/%s_android.log"%(log_path,project)
    os.system("%s;make -j`grep processor /proc/cpuinfo | wc -l` 2>&1 | tee -a %s"%(cmd,log2))
    f = open(log2)
    lines = f.readlines()
    f.close()
    if "successfully" in lines[-2]:
        wt_print("m","Successed!")
    else:
        wt_print("e","Build Android ERROR! Check log @ android/%s"%log2)
    
    if ds1["ota"] == True:
        log3 = "%s/%s_otapackage.log"%(log_path,project)
        os.system("%s;make -j`grep processor /proc/cpuinfo | wc -l` otapackage 2>&1 | tee -a %s"%(cmd,log3))
        f = open(log3)
        lines = f.readlines()
        f.close()
        if "successfully" in lines[-2]:
            wt_print("m","Successed!")
        else:
            wt_print("e","Build otapackage ERROR! Check log @ %s"%log3)

def splitandrepack():
    os.chdir("%s/android"%root)
    wt_print("m","Begin to generate qcom-format package ...")
    otpp = "out/target/product/%s"%project
    wt_remkd("%s/fullbuild"%otpp)
    wt_remkd("%s/images"%otpp)
    wt_sys("ln -s ../boot.img  %s/fullbuild"%otpp)
    wt_sys("ln -s ../recovery.img %s/fullbuild"%otpp)
    wt_sys("ln -s ../emmc_appsboot.mbn %s/fullbuild"%otpp)
    wt_sys("ln -s ../emmc_appsboot.mbn %s/images"%otpp)
    wt_sys("cp -f ../custom.img %s/fullbuild"%otpp)
    wt_sys("cp -f ../custom.img %s/images"%otpp)
    wt_sys("ln -s ../obj/KERNEL_OBJ/vmlinux %s/fullbuild"%otpp)
    wt_sys("ln -s ../obj/KERNEL_OBJ/vmlinux %s/images"%otpp)
    wt_sys("ln -s ../mdtp.img %s/fullbuild"%otpp)
    mk_version_txt()
    os.chdir("%s/android"%root)
    if os.path.exists("out/LVP_applist.csv"):
        wt_sys("cp out/LVP_applist.csv %s/fullbuild"%otpp)
    if os.path.exists("out/LVP_applist_GB.csv"):
        wt_sys("cp out/LVP_applist_GB.csv %s/fullbuild"%otpp)
    vwbt = "../../../../../vendor/wingtech/build/tools"
    date1 = time.strftime("%Y-%m-%d")
    time1 = time.strftime("%H%M%S")
    if project == "wt86528_C1" or project == "wt86518_C1" or project == "wt82918_B1" or project == "wt86528_C2" or project == "wt86528_C3" or project == "wt82918_F1" or project == "wt82918_G1" or project == "wt82918_D1" or project == "wt82918_E1":
        if os.path.isfile("%s/userdata.img_bak"%otpp) and not os.path.isfile("%s/without_carrier_userdata/without_carrier_userdata_1.img"%otpp):
            os.system("cd %s ;rm -rf without_carrier_userdata ;mkdir -p without_carrier_userdata ; cd without_carrier_userdata ; cp ../userdata.img_bak without_carrier_userdata.img ; cp ../cache.img without_carrier_cache.img ; echo \<?xml version=\\\"1.0\\\" ?\> > rawprogram0.xml ; echo \<data\> >> rawprogram0.xml ; grep cache.img %s/scripts/rawprogram0.xml -rw | sed s/cache.img/without_carrier_cache.img/ >>  rawprogram0.xml ; grep userdata %s/scripts/rawprogram0.xml -rw | sed s/userdata.img/without_carrier_userdata.img/ >>  rawprogram0.xml ; echo \</data\> >>  rawprogram0.xml; python %s/scripts/checksparse.py -i rawprogram0.xml -o rawprogram_unsparse_clean_carrier.xml -s . ; rm without_carrier_userdata.img *.xml.bak; cd ../fullbuild ; mv ../without_carrier_userdata/rawprogram0.xml rawprogram0_clean_carrier.xml ; ln -s ../without_carrier_userdata/* . "%(otpp,vwbt,vwbt,vwbt))
            if not os.path.isfile("%s/without_carrier_userdata/without_carrier_userdata_1.img"%otpp):
                print "Error: split without_carrier_userdata.img , rerun with ./wt publish xxx -p xxx -c xxx -v"
                sys.eixt(-1)
    wt_sys("cd %s/fullbuild && python %s/scripts/checksparse.py -i %s/scripts/rawprogram0.xml -o rawprogram_unsparse.xml -s %s/binary -s ../ && ln -s %s/binary/* . && qcn_file=`ls | grep Online | grep img | tail -n 1` && sed -i \"s/filename=\\\"\\\" label=\\\"fsg\\\"/filename=\\\"$qcn_file\\\" label=\\\"fsg\\\"/g\" rawprogram_unsparse.xml && if test -f  %s/binary/rawprogram_unsparse_namechange_config ; then python %s/android/tools/wingtechtools/python/tcl_rawprogram_unsparse_tool.py %s/binary/rawprogram_unsparse_namechange_config rawprogram_unsparse.xml wt_rawprogram_unsparse.xml  ; fi ; rm -rf checksum ; mkdir checksum ; for i in `find -type f -o -type l` ; do mkdir -p `dirname checksum/$i`; md5sum $i | cut -d\" \" -f1 > checksum/$i.md5  ; done && zip -r ../%s_%s_LRX22G_%s-%s.zip * -x rawprogram0.xml.bak"%(otpp,vwbt,vwbt,vwbt,vwbt,vwbt,root,vwbt,project,mode,date1,time1))
    wt_sys("cp %s/*.img  %s/images"%(otpp,otpp))
    wt_sys("cp %s/fullbuild/rawprogram0.xml.bak  %s/images/rawprogram0.xml"%(otpp,otpp))
    wt_sys("cd %s/images && ln -s %s/binary/* . ; rm -rf checksum ; mkdir checksum ; for i in `find -type f -o -type l` ; do mkdir -p `dirname checksum/$i`; md5sum $i | cut -d\" \" -f1 > checksum/$i.md5  ; done && cd .. && zip -r %s_images_%s_LRX22G_%s-%s.zip ./images -x rawprogram0.xml.bak"%(otpp,vwbt,project,mode,date1,time1))
    wt_sys("cd %s && zip -rj %s_images_%s_LRX22G_%s-%s.zip ../../../../vendor/wingtech/build/tools/fastboot/*"%(otpp,project,mode,date1,time1))
    wt_print("m","Generate qcom-format package successed!")

def packagesoft():
    os.chdir(root)
    wt_remkd("%s/software"%ver_inn)
    wt_sys("cp android/out/target/product/%s/*LRX22G* %s/software"%(project,ver_inn))
    wt_sys("rm %s/software/*images*"%ver_inn)
    wt_remkd("%s/debug"%ver_inn)
    wt_sys("cp android/out/target/product/%s/obj/KERNEL_OBJ/vmlinux %s/debug"%(project,ver_inn))
    wt_sys("cp android/out/target/product/%s/symbols symbols -af"%project)
    wt_sys("cp android/vendor/wingtech/build/tools/symbol/* symbols/.")
    wt_sys("cp android/out/target/product/%s/obj/vendor/qcom/opensource symbols/. -raf"%project)
    wt_sys("zip -r -m %s/debug/symbols.zip symbols"%ver_inn)
    

def packageqmss():
    os.chdir(root)
    wt_print("m","PACKING: qmss")
    wt_remkd("%s/qmss"%ver_inn)
    wt_sys("cp android/vendor/wingtech/build/tools/binary/*elf* %s/qmss/."%ver_inn)
    wt_successed()

def packageota():
    os.chdir(root)
    wt_print("m","PACKING: otafull")
    wt_remkd("%s/otafull"%ver_inn)
    if project.startswith("wt86001"):
        OFTF_A="source build/envsetup.sh ; lunch %s-%s ; "%(project,mode) + OFTF
        wt_sys("cp android/out/target/product/%s/obj/PACKAGING/target_files_intermediates/*.zip android/target.zip"%project)
        wt_sys("cd android;%s -k build/target/product/security/wtcommon/releasekey -w target.zip update.zip;cd -"%OFTF_A)
        wt_sys("cp android/target.zip %s/otafull/base.zip"%ver_inn)
        wt_sys("cp android/update.zip %s/otafull/update.zip"%ver_inn)
    elif project.startswith("wt86621") or project.startswith("wt86611AA1") or project.startswith("wt89536") :
        wt_sys("cp android/out/target/product/%s/%s-ota-*.zip %s/otafull/update.zip"%(project,project,ver_inn))
        wt_sys("cp android/out/target/product/%s/obj/PACKAGING/target_files_intermediates/*.zip %s/otafull/base.zip"%(project,ver_inn))
    elif project.startswith("wt82918tra"):
        wt_sys("cp android/out/target/product/%s/target_files-package.zip %s/otafull/base.zip"%(project,ver_inn))
    else:
        wt_sys("cp android/out/target/product/%s/obj/PACKAGING/target_files_intermediates/*.zip %s/otafull/update.zip"%(project,ver_inn))
    wt_successed()

def make_multipackage_ota():
    os.chdir(root)
    wt_print("m","PACKING: make multipackage_ota")
    os.chdir("%s/android"%root)

    newzipdir = "android/out/target/product/%s/obj/PACKAGING/target_files_intermediates"%project
    wt_sys("cp %s/%s-target_*.zip android/new.zip"%(newzipdir,project))

    wt_sys("rm -rf tmp_ota_diff_new tmp_ota_diff_new_kk2l tmp_ota_diff_new_l2l ; mkdir tmp_ota_diff_new  tmp_ota_diff_new_kk2l tmp_ota_diff_new_l2l")
    wt_sys("unzip new.zip DATA/media/carrier/*.zip -d tmp_ota_diff_new")
    wt_sys("cp -rf tmp_ota_diff_new/* tmp_ota_diff_new_kk2l/")
    wt_sys("cp -rf tmp_ota_diff_new/* tmp_ota_diff_new_l2l/")
    
    carrier_dir="DATA/media/carrier"
    update_script_file="META-INF/com/google/android/updater-script"
    # charege tmp_ota_diff_new
    # charege tmp_ota_diff_new_kk2l
    new_kk2l_carrier_zips=os.listdir("tmp_ota_diff_new_kk2l/"+carrier_dir)
    for i in new_kk2l_carrier_zips:
        wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_new_kk2l/%s/%s -d tmp_ota_diff_new_kk2l/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
        abs_update_script_file="tmp_ota_diff_new_kk2l/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file
        if os.path.isfile(abs_update_script_file):
            wt_print("m","exist file : tmp_ota_diff_new_kk2l/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
            wt_sys("sed -i 's#getprop(\"ro.build.fingerprint\") == #\#getprop(\"ro.build.fingerprint\") == #' %s"%(abs_update_script_file))
            wt_sys("sed -i 's#abort(\"Package expects build fingerprint of#\#abort(\"Package expects build fingerprint of#' %s"%(abs_update_script_file))
            wt_sys("sed -i 's#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#\#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#' %s"%(abs_update_script_file))
            os.system("sed -i '/set_progress(1.00/d' %s"%(abs_update_script_file))
            os.system("echo mount\(\"\\\"ext4\\\"\", \"\\\"EMMC\\\"\", \"\\\"/dev/block/bootdevice/by-name/userdata\\\"\", \"\\\"/data\\\"\"\)\;>>%s"%(abs_update_script_file))
            os.system("echo delete\(\"\\\"/data/data/com.android.launcher3/cache/com.android.opengl.shaders_cache\\\"\",>>%s"%(abs_update_script_file))
            os.system("echo   \"      \\\"/data/data/com.android.launcher3/databases/launcher.db\\\"\",>>%s"%(abs_update_script_file))
            os.system("echo   \"      \\\"/data/data/com.android.launcher3/databases/launcher.db-journal\\\"\",>>%s"%(abs_update_script_file))
            os.system("echo   \"      \\\"/data/data/com.android.launcher3/files/launcher.preferences\\\"\",>>%s"%(abs_update_script_file))
            os.system("echo   \"      \\\"/data/data/com.android.launcher3/shared_prefs/com.android.launcher2.prefs.xml\\\"\",>>%s"%(abs_update_script_file))
            os.system("echo   \"      \\\"/data/data/com.android.launcher3/shared_prefs/launcher.preferences.almostnexus.xml\\\"\"\)\;>>%s"%(abs_update_script_file))
            os.system('echo \"set_progress(1.000000);\" >>%s'%(abs_update_script_file))
        else:
            wt_print("e","not exist file : tmp_ota_diff_new_kk2l/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
        wt_sys("cd tmp_ota_diff_new_kk2l/%s/%s ; zip -u -r ../%s * ; cd - ;  rm -rf tmp_ota_diff_new_kk2l/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))
    # charege tmp_ota_diff_new_l2l
    new_l2l_carrier_zips=os.listdir("tmp_ota_diff_new_l2l/"+carrier_dir)
    for i in new_l2l_carrier_zips:
        wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_new_l2l/%s/%s -d tmp_ota_diff_new_l2l/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
        abs_update_script_file="tmp_ota_diff_new_l2l/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file
        if os.path.isfile(abs_update_script_file):
            wt_print("m","exist file : tmp_ota_diff_new_l2l/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
            wt_sys("sed -i 's#getprop(\"ro.build.fingerprint\") == #\#getprop(\"ro.build.fingerprint\") == #' %s"%(abs_update_script_file))
            wt_sys("sed -i 's#abort(\"Package expects build fingerprint of#\#abort(\"Package expects build fingerprint of#' %s"%(abs_update_script_file))
            wt_sys("sed -i 's#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#\#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#' %s"%(abs_update_script_file))
        else:
            wt_print("e","not exist file : tmp_ota_diff_new_l2l/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
        wt_sys("cd tmp_ota_diff_new_l2l/%s/%s ; zip -u -r ../%s * ; cd - ;  rm -rf tmp_ota_diff_new_l2l/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))
        
    abs_multipackage_ota_system_zip_path_kk="vendor/qcom/proprietary/qrdplus/Extension/config/MultiOTAKK"
    #kk to l multipackage_ota.zip  UpdateSystemMultiOTA  tmp_ota_diff_new_kk2l/*.ota.zip
    if os.path.isdir(abs_multipackage_ota_system_zip_path_kk):
        print "=========generate kk to l  multipackage_ota.zip=============="
        package_ota_file_tmp=wt_get("ls -rt out/target/product/%s/%s-ota*.zip |tail -n 1"%(project,project))
        if not os.path.isfile(package_ota_file_tmp):
            wt_print("e","not exist file : out/target/product/" + project + "/" + project +"-ota-xxxxx.zip ")
        wt_sys("rm -rf  %s/*.zip ; cp %s %s/System.zip"%(abs_multipackage_ota_system_zip_path_kk,package_ota_file_tmp,abs_multipackage_ota_system_zip_path_kk))
        print "list dir step1 %s :%s"%(abs_multipackage_ota_system_zip_path_kk,os.listdir(abs_multipackage_ota_system_zip_path_kk))
        wt_sys("cp tmp_ota_diff_new_kk2l/%s/*.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path_kk))
        print "list dir step2 %s :%s"%(abs_multipackage_ota_system_zip_path_kk,os.listdir(abs_multipackage_ota_system_zip_path_kk))
        wt_sys("rm -rf  %s/*2Default.ota.zip"%(abs_multipackage_ota_system_zip_path_kk))
        print "list dir step3 %s :%s"%(abs_multipackage_ota_system_zip_path_kk,os.listdir(abs_multipackage_ota_system_zip_path_kk))
        print "=========make UpdateSystemMultiOTA=============="
        wt_sys("export DISABLE_AUTO_INSTALLCLEAN=true ; export WT_LENOVO_CARRIER_ALL=%s ; source build/envsetup.sh ; lunch %s-%s ; make UpdateSystemMultiOTA"%(ds1["carrier-all"],project,mode))
        print "list dir step4 %s :%s"%(abs_multipackage_ota_system_zip_path_kk,os.listdir(abs_multipackage_ota_system_zip_path_kk))
        if not os.path.isfile(abs_multipackage_ota_system_zip_path_kk + "/multipackage_ota.zip"):
            wt_print("e","not exist UpdateSystemMultiOTA file multipackage_ota.zip: " + abs_multipackage_ota_system_zip_path_kk + "/multipackage_ota.zip")
        wt_sys("rm -rf out/target/product/%s/KK2L_multipackage_ota.zip; mv %s/multipackage_ota.zip out/target/product/%s/KK2L_multipackage_ota.zip"%(project,abs_multipackage_ota_system_zip_path_kk,project))
        print "list dir step5 %s :%s"%(abs_multipackage_ota_system_zip_path_kk,os.listdir(abs_multipackage_ota_system_zip_path_kk))
    else:
        print "=========skip generate kk to l  multipackage_ota.zip   not exist %s=============="%abs_multipackage_ota_system_zip_path_kk

    abs_multipackage_ota_system_zip_path="vendor/qcom/proprietary/qrdplus/Extension/config/MultiOTA"
    #l to l multipackage_ota.zip  MultiOTA  tmp_ota_diff_new_l2l/*2Default.ota.zip  tmp_ota_diff_new_l2l/!*2Default.ota.zip
    print "=========generate l to l  multipackage_ota.zip=============="
    package_ota_file_tmp=wt_get("ls -rt out/target/product/%s/%s-ota*.zip |tail -n 1"%(project,project))
    if not os.path.isfile(package_ota_file_tmp):
        wt_print("e","not exist file : out/target/product/" + project + "/" + project +"-ota-xxxxx.zip ")
    wt_sys("rm -rf  %s/*.zip ; cp %s %s/System.zip"%(abs_multipackage_ota_system_zip_path,package_ota_file_tmp,abs_multipackage_ota_system_zip_path))
    print "list dir step1 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
    wt_sys("cp tmp_ota_diff_new_l2l/%s/*.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path))
    print "list dir step2 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
    wt_sys("rm -rf  %s/*2Default.ota.zip"%(abs_multipackage_ota_system_zip_path))
    print "list dir step3 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
    # wt_sys("cp -f tmp_ota_diff_old/%s/*2Default.ota.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path))
    print "list dir step4 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
    print "=========make MultiOTA=============="
    wt_sys("export DISABLE_AUTO_INSTALLCLEAN=true ; export WT_LENOVO_CARRIER_ALL=%s ; source build/envsetup.sh ; lunch %s-%s ; make MultiOTA"%(ds1["carrier-all"],project,mode))
    print "list dir step5 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
    if not os.path.isfile(abs_multipackage_ota_system_zip_path + "/multipackage_ota.zip"):
        wt_print("e","not exist MultiOTA file multipackage_ota.zip: " + abs_multipackage_ota_system_zip_path + "/multipackage_ota.zip")
    wt_sys("rm -rf out/target/product/%s/L2L_multipackage_ota.zip; mv %s/multipackage_ota.zip out/target/product/%s/L2L_multipackage_ota.zip"%(project,abs_multipackage_ota_system_zip_path,project))
    print "list dir step6 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
                    
def packagediff():
    os.chdir(root)
    wt_print("m","PACKING: otadiff")
    wt_remkd("%s/otadiff"%ver_inn)
    if not os.path.isfile("android/new.zip"):
        newzipdir = "android/out/target/product/%s/obj/PACKAGING/target_files_intermediates"%project
        wt_sys("cp %s/%s-target_*.zip android/new.zip"%(newzipdir,project))
    old_ota_version_file = "android/wingcust/%s/%s/manifest/old_ota_versions"%(ds1["project"],custdir)
    out = wt_get("cat %s"%old_ota_version_file).split()
    os.chdir("%s/android"%root)
    if ds1["include-carrier"] == True:
        make_multipackage_ota()
        
        carrier_dir="DATA/media/carrier"
        update_script_file="META-INF/com/google/android/updater-script"
        #find new_fingerprint charege
        new_carrier_zips=os.listdir("tmp_ota_diff_new/"+carrier_dir)
        for i in new_carrier_zips:
            wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_new/%s/%s -d tmp_ota_diff_new/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
        new_fingerprint_file="tmp_ota_diff_new/" +carrier_dir + "/" + i[:-4] +"/" + update_script_file
        f = open(new_fingerprint_file)
        lines = f.readlines()
        for i in lines:
            if "ro.build.fingerprint" in i:
                new_fingerprint=i.split('==')[1].split('\"')[1]
                print "new_fingerprint:%s"%new_fingerprint
                break
        for i in new_carrier_zips:
            wt_sys("cd tmp_ota_diff_new/%s/%s ; zip -u -r ../%s * ; cd - ;  rm -rf tmp_ota_diff_new/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))

    for one in out:
        os.system("rm -rf old_version.txt version.txt")
        wt_ftp("download","PUBLISH/%s_ota:."%one)
        if project.startswith("wt86001"):
            tcl_old_ota_version=""
            tcl_current_ota_version=""
            ro_tct_sys_ver_old=""
            ro_tct_sys_ver_new=""
            if os.path.isfile('old_version.txt'):
                tcl_old_ota_version=wt_get('grep -ir WT_TCL_CUSTOMER_SVN old_version.txt').replace('WT_TCL_CUSTOMER_SVN=','').replace('V','').strip()
                ro_tct_sys_ver_old=wt_get('grep -ir ro_tct_sys_ver old_version.txt').replace('ro_tct_sys_ver=','').strip()
            if os.path.isfile('current_version.txt'):
                tcl_current_ota_version=wt_get('grep -ir WT_TCL_CUSTOMER_SVN current_version.txt').replace('WT_TCL_CUSTOMER_SVN=','').replace('V','').strip()
                ro_tct_sys_ver_new=wt_get('grep -ir ro_tct_sys_ver current_version.txt').replace('ro_tct_sys_ver=','').strip()
            if len(tcl_current_ota_version) > 0:
                if len(tcl_old_ota_version) == 0 :
                    tcl_old_ota_version='00000'
            if len(ro_tct_sys_ver_new) > 0:
                if len(ro_tct_sys_ver_old) == 0 :
                    ro_tct_sys_ver_old='00000000'

        OFTF_A="source build/envsetup.sh ; lunch %s-%s ; "%(project,mode) + OFTF
        if ds1["lenovo"] == True:
            lenovo_key = "build/target/product/security/lenovo/releasekey"
            cmdpre = "%s -v --block -p out/host/linux-x86 -k %s -i "%(OFTF_A,lenovo_key)
        else:
            if project.startswith("wt86001") or project.startswith("wt86621") or project.startswith("wt86611AA1") or project.startswith("wt89536") :
                test_key = "build/target/product/security/wtcommon/releasekey"
                cmdpre = "%s -p out/host/linux-x86 -k %s -v --block -i "%(OFTF_A,test_key)
            else:
                test_key = "build/target/product/security/testkey"
                cmdpre = "%s -p out/host/linux-x86 -k %s -v --block -i "%(OFTF_A,test_key)
        if ds1["include-carrier"] == True:
            wt_sys("rm -rf tmp_ota_diff_old tmp_ota_diff_new_oldfpt tmp_ota_diff_old_newfpt ; mkdir tmp_ota_diff_old tmp_ota_diff_new_oldfpt tmp_ota_diff_old_newfpt")
            wt_sys("unzip old.zip DATA/media/carrier/*.zip -d tmp_ota_diff_old")
            wt_sys("cp -rf tmp_ota_diff_new/* tmp_ota_diff_new_oldfpt/")
            wt_sys("cp -rf tmp_ota_diff_old/* tmp_ota_diff_old_newfpt/")
            
            carrier_dir="DATA/media/carrier"
            update_script_file="META-INF/com/google/android/updater-script"
            #find old_fingerprint  charege tmp_ota_diff_old_newfpt
            old_newfpt_carrier_zips=os.listdir("tmp_ota_diff_old_newfpt/"+carrier_dir)
            for i in old_newfpt_carrier_zips:
                wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_old_newfpt/%s/%s -d tmp_ota_diff_old_newfpt/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
            old_fingerprint_file="tmp_ota_diff_old_newfpt/" +carrier_dir + "/" + i[:-4] +"/" + update_script_file
            f = open(old_fingerprint_file)
            lines = f.readlines()
            for i in lines:
                if "ro.build.fingerprint" in i:
                    old_fingerprint=i.split('==')[1].split('\"')[1]
                    print "old_fingerprint:%s"%old_fingerprint
                    break
            # charege tmp_ota_diff_old
            old_carrier_zips=os.listdir("tmp_ota_diff_old/"+carrier_dir)
            for i in old_carrier_zips:
                wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_old/%s/%s -d tmp_ota_diff_old/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
            #change tmp_ota_diff_new_oldfpt dir  to old_fingerprint
            new_oldfpt_carrier_zips=os.listdir("tmp_ota_diff_new_oldfpt/"+carrier_dir)
            for i in new_oldfpt_carrier_zips:
                wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_new_oldfpt/%s/%s -d tmp_ota_diff_new_oldfpt/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
                abs_update_script_file="tmp_ota_diff_new_oldfpt/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file
                if os.path.isfile(abs_update_script_file):
                    wt_print("m","exist file : tmp_ota_diff_new_oldfpt/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                    wt_sys("sed -i 's#^getprop(\"ro.build.fingerprint\") == \"%s\"#getprop(\"ro.build.fingerprint\") == \"%s\"#' %s"%(new_fingerprint,old_fingerprint,abs_update_script_file))
                    wt_sys("sed -i 's#abort(\"Package expects build fingerprint of %s#abort(\"Package expects build fingerprint of %s#' %s"%(new_fingerprint,old_fingerprint,abs_update_script_file))
                    wt_sys("sed -i 's#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#\#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#' %s"%(abs_update_script_file))
                else:
                    wt_print("e","not exist file : tmp_ota_diff_new_oldfpt/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                wt_sys("cd tmp_ota_diff_new_oldfpt/%s/%s ; zip -u -r ../%s * ; cd - ;  rm -rf tmp_ota_diff_new_oldfpt/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))
            #change tmp_ota_diff_old_newfpt dir  to new_fingerprint
            for i in old_newfpt_carrier_zips:
                abs_update_script_file="tmp_ota_diff_old_newfpt/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file
                if os.path.isfile(abs_update_script_file):
                    wt_print("m","exist file : tmp_ota_diff_old_newfpt/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                    wt_sys("sed -i 's#^getprop(\"ro.build.fingerprint\") == \"%s\"#getprop(\"ro.build.fingerprint\") == \"%s\"#' %s"%(old_fingerprint,new_fingerprint,abs_update_script_file))
                    wt_sys("sed -i 's#abort(\"Package expects build fingerprint of %s#abort(\"Package expects build fingerprint of %s#' %s"%(old_fingerprint,new_fingerprint,abs_update_script_file))
                    wt_sys("sed -i 's#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#\#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#' %s"%(abs_update_script_file))
                else:
                    wt_print("e","not exist file : tmp_ota_diff_old_newfpt/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                wt_sys("cd tmp_ota_diff_old_newfpt/%s/%s ; zip -u -r ../%s * ; cd - ; rm -rf tmp_ota_diff_old_newfpt/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))
            #change tmp_ota_diff_new dir  to old_fingerprint
            new_carrier_zips=os.listdir("tmp_ota_diff_new/"+carrier_dir)
            for i in new_carrier_zips:
                wt_sys("mkdir -p %s/%s ; unzip tmp_ota_diff_new/%s/%s -d tmp_ota_diff_new/%s/%s"%(carrier_dir,i[:-4],carrier_dir,i,carrier_dir,i[:-4]))
                abs_update_script_file="tmp_ota_diff_new/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file
                if os.path.isfile(abs_update_script_file):
                    wt_print("m","exist file : tmp_ota_diff_new/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                    # wt_sys("sed -i 's#^getprop(\"ro.build.fingerprint\") == \"%s\"#getprop(\"ro.build.fingerprint\") == \"%s\"#' %s"%(new_fingerprint,old_fingerprint,abs_update_script_file))
                    # wt_sys("sed -i 's#abort(\"Package expects build fingerprint of %s#abort(\"Package expects build fingerprint of %s#' %s"%(new_fingerprint,old_fingerprint,abs_update_script_file))
                    wt_sys("sed -i 's#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#\#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#' %s"%(abs_update_script_file))
                else:
                    wt_print("e","not exist file : tmp_ota_diff_new/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                wt_sys("cd tmp_ota_diff_new/%s/%s ; zip -u -r ../%s * ; cd - ;  rm -rf tmp_ota_diff_new/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))
            #change tmp_ota_diff_old dir  to new_fingerprint
            for i in old_carrier_zips:
                abs_update_script_file="tmp_ota_diff_old/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file
                if os.path.isfile(abs_update_script_file):
                    wt_print("m","exist file : tmp_ota_diff_old/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                    # wt_sys("sed -i 's#^getprop(\"ro.build.fingerprint\") == \"%s\"#getprop(\"ro.build.fingerprint\") == \"%s\"#' %s"%(old_fingerprint,new_fingerprint,abs_update_script_file))
                    # wt_sys("sed -i 's#abort(\"Package expects build fingerprint of %s#abort(\"Package expects build fingerprint of %s#' %s"%(old_fingerprint,new_fingerprint,abs_update_script_file))
                    wt_sys("sed -i 's#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#\#format(\"ext4\", \"EMMC\", \"/dev/block/bootdevice/by-name/userdata#' %s"%(abs_update_script_file))
                else:
                    wt_print("e","not exist file : tmp_ota_diff_old/"+carrier_dir+"/"+i[:-4]+"/"+update_script_file)
                wt_sys("cd tmp_ota_diff_old/%s/%s ; zip -u -r ../%s * ; cd - ; rm -rf tmp_ota_diff_old/%s/%s "%(carrier_dir,i[:-4],i,carrier_dir,i[:-4]))
                
            abs_multipackage_ota_system_zip_path="vendor/qcom/proprietary/qrdplus/Extension/config/MultiOTA"
            #update.zip  UpdateCarrierMultiOTA  tmp_ota_diff_old_newfpt/*2Default.ota.zip  tmp_ota_diff_new/!*2Default.ota.zip
            print "=========generate update.zip=============="
            wt_sys("%s old.zip new.zip update.zip"%cmdpre)
            if not os.path.exists("update.zip"):
                wt_print("e","Generate OTA different package Failed : From %s To %s"%(one,ver_inn))
            wt_sys("rm -rf  %s/*.zip ; mv update.zip %s/System.zip"%(abs_multipackage_ota_system_zip_path,abs_multipackage_ota_system_zip_path))
            print "list dir step1 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            wt_sys("cp tmp_ota_diff_new_oldfpt/%s/*.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path))
            print "list dir step2 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            wt_sys("rm -rf  %s/*2Default.ota.zip"%(abs_multipackage_ota_system_zip_path))
            print "list dir step3 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            wt_sys("cp -f tmp_ota_diff_old/%s/*2Default.ota.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path))
            print "list dir step4 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            print "=========make UpdateCarrierMultiOTA=============="
            wt_sys("export DISABLE_AUTO_INSTALLCLEAN=true ; export WT_LENOVO_CARRIER_ALL=%s ; source build/envsetup.sh ; lunch %s-%s ; make UpdateCarrierMultiOTA"%(ds1["carrier-all"],project,mode))
            print "list dir step5 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            if not os.path.isfile(abs_multipackage_ota_system_zip_path + "/multipackage_ota.zip"):
                wt_print("e","not exist UpdateCarrierMultiOTA file update.zip: " + abs_multipackage_ota_system_zip_path + "/multipackage_ota.zip")
            wt_sys("mv %s/multipackage_ota.zip update.zip"%abs_multipackage_ota_system_zip_path)
            print "list dir step6 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            
            #goback.zip  UpdateCarrierMultiOTA  tmp_ota_diff_new_oldfpt/*2Default.ota.zip  tmp_ota_diff_old/!*2Default.ota.zip
            print "=========generate goback.zip=============="
            wt_sys("%s new.zip old.zip goback.zip"%cmdpre)
            if not os.path.exists("goback.zip"):
                wt_print("e","Generate OTA different package Failed : From %s To %s"%(ver_inn,one))
            wt_sys("rm -rf  %s/*.zip ; mv goback.zip %s/System.zip"%(abs_multipackage_ota_system_zip_path,abs_multipackage_ota_system_zip_path))
            print "list dir step1 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            wt_sys("cp tmp_ota_diff_old_newfpt/%s/*.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path))
            print "list dir step2 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            wt_sys("rm -rf  %s/*2Default.ota.zip"%(abs_multipackage_ota_system_zip_path))
            print "list dir step3 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            wt_sys("cp -f tmp_ota_diff_new/%s/*2Default.ota.zip %s/"%(carrier_dir,abs_multipackage_ota_system_zip_path))
            print "list dir step4 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            print "=========make UpdateCarrierMultiOTA=============="
            wt_sys("export DISABLE_AUTO_INSTALLCLEAN=true ; export WT_LENOVO_CARRIER_ALL=%s ; source build/envsetup.sh ; lunch %s-%s ; make UpdateCarrierMultiOTA"%(ds1["carrier-all"],project,mode))
            print "list dir step5 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
            if not os.path.isfile(abs_multipackage_ota_system_zip_path + "/multipackage_ota.zip"):
                wt_print("e","not exist UpdateCarrierMultiOTA file goback.zip: " + abs_multipackage_ota_system_zip_path + "/multipackage_ota.zip")
            wt_sys("mv %s/multipackage_ota.zip goback.zip"%abs_multipackage_ota_system_zip_path)
            print "list dir step6 %s :%s"%(abs_multipackage_ota_system_zip_path,os.listdir(abs_multipackage_ota_system_zip_path))
        else:
            if project.startswith("wt86001") and len(tcl_current_ota_version) > 0:
                os.system("echo %s > version.txt ; echo %s >> version.txt "%(ro_tct_sys_ver_old,ro_tct_sys_ver_new))
            wt_sys("%s old.zip new.zip update.zip"%cmdpre)
            if project.startswith("wt86001") and len(tcl_current_ota_version) > 0:
                os.system("echo %s > version.txt ; echo %s >> version.txt "%(ro_tct_sys_ver_new,ro_tct_sys_ver_old))
            wt_sys("%s new.zip old.zip goback.zip"%cmdpre)
            if not os.path.exists("update.zip"):
                wt_print("e","Generate OTA different package Failed : From %s To %s"%(one,ver_inn))
            if not os.path.exists("goback.zip"):
                wt_print("e","Generate OTA different package Failed : From %s To %s"%(ver_inn,one))
        if project.startswith("wt86001") and len(tcl_current_ota_version) > 0:
            if tcl_old_ota_version == tcl_current_ota_version:
                wt_sys("cp update.zip ../%s/otadiff/ODM_I1_6044D_%s_to_%s_USER_incrementOTA.zip"%(ver_inn,tcl_old_ota_version,tcl_current_ota_version))
                wt_sys("cp goback.zip ../%s/otadiff/ODM_I1_6044D_%s_to_%s_USER_incrementOTA_goback.zip"%(ver_inn,tcl_current_ota_version,tcl_old_ota_version))
            else:
                wt_sys("cp update.zip ../%s/otadiff/ODM_I1_6044D_%s_to_%s_USER_incrementOTA.zip"%(ver_inn,tcl_old_ota_version,tcl_current_ota_version))
                wt_sys("cp goback.zip ../%s/otadiff/ODM_I1_6044D_%s_to_%s_USER_incrementOTA.zip"%(ver_inn,tcl_current_ota_version,tcl_old_ota_version))
        else:
            wt_sys("cp update.zip ../%s/otadiff/update_%s_to_%s.zip"%(ver_inn,one,ver_inn))
            wt_sys("cp goback.zip ../%s/otadiff/goback_%s_to_%s.zip"%(ver_inn,ver_inn,one))
        wt_sys("rm -f update.zip goback.zip old.zip")
    wt_successed()


def packagenote():
    os.chdir(root)
    wt_print("m","PACKING: release-note")
    wt_mkdir("%s/doc"%ver_inn)
    os.system("repo forall -pc git log --after={2014-09-01} --date=iso >tmp.txt")
    if os.path.exists("android/wingcust/%s/%s/manifest/oldmanifest"%(ds1["project"],custdir)):
        if len(gen_lines("android/wingcust/%s/%s/manifest/oldmanifest"%(ds1["project"],custdir))) !=0:
            oldmanifest = gen_lines("android/wingcust/%s/%s/manifest/oldmanifest"%(ds1["project"],custdir))[0][:-1]
            oldmanifest = "android/wingcust/%s/%s/manifest/%s"%(ds1["project"],custdir,oldmanifest)
            if not os.path.isfile(oldmanifest):
                oldmanifest=""
        else:
            oldmanifest=""
    else:
        oldmanifest = ""
    if "_V001_" in ver_inn or oldmanifest == "":     
        wt_sys("perl android/tools/wingtechtools/createlog/createlog.pl tmp.txt %s/doc/Release_notes.xls"%ver_inn)
    else:
        wt_sys("perl android/tools/wingtechtools/createlog/createlog.pl tmp.txt %s/doc/Release_notes.xls %s"%(ver_inn,oldmanifest))
    wt_sys("rm tmp.txt")
    wt_successed()

def record():
    os.chdir(root)
    wt_mkdir("%s/doc"%ver_inn)
    wt_print("m","Record: manifest.xml")
    wt_sys("repo manifest -r -o %s/doc/manifest_%s.xml"%(ver_inn,ver_inn))
    wt_mkdir("android/wingcust/%s/%s/manifest")
    wt_sys("cp %s/doc/manifest_%s.xml android/wingcust/%s/%s/manifest/."%(ver_inn,ver_inn,ds1["project"],custdir))
    wt_print("m","Save manifest.xml to android/wingcust/%s/%s/manifest!"%(ds1["project"],custdir))
    wt_print("m","Record: oldmanifest")
    f = open("android/wingcust/%s/%s/manifest/oldmanifest"%(ds1["project"],custdir),"w")
    f.write("manifest_%s.xml"%ver_inn)
    f.close()
    wt_print("m","Save oldmanifest to android/wingcust/%s/%s/manifest!"%(ds1["project"],custdir))
    wt_print("m","Record: old_ota_versions")
    os.system("echo %s >> android/wingcust/%s/%s/manifest/old_ota_versions"%(ver_inn,ds1["project"],custdir))
    wt_print("m","Save old_ota_versions to android/wingcust/%s/%s/manifest!"%(ds1["project"],custdir))
    rbranch = gen_remote_branch()
    wt_sys("cd android/wingcust/%s;git add %s/manifest;git commit -n -m 'RECORD MANIFEST';git push aosp HEAD:refs/for/%s;cd -"%(ds1["project"],custdir,rbranch))
    wt_print("m","Commit to gerrit server,need to review : android/wingcust/%s"%ds1["project"])

def package():
    os.chdir(root)
    wt_print("m","Begin to package files...")
    wt_sys("rm -rf %s"%ver_inn)
    wt_sys("mkdir %s"%ver_inn)
    packagesoft()
    if ds1["qmss"] == True:
        packageqmss()
    if ds1["ota"] == True:
        packageota()
    if ds1["otadiff"] == True:
        packagediff()
    if ds1["releasenote"] == True:
        packagenote()

def repackage_lenovo():
    if os.path.exists("android/out/target/product/%s/KK2L_multipackage_ota.zip"%project): 
        wt_sys("cp android/out/target/product/%s/KK2L_multipackage_ota.zip %s/otafull/KK2L_multipackage_ota.zip"%(project,ver_inn))
    if os.path.exists("android/out/target/product/%s/L2L_multipackage_ota.zip"%project):
        wt_sys("cp android/out/target/product/%s/L2L_multipackage_ota.zip %s/otafull/L2L_multipackage_ota.zip"%(project,ver_inn))
    if not os.path.exists("android/out/target/product/%s/KK2L_multipackage_ota.zip"%project) and not os.path.exists("android/out/target/product/%s/L2L_multipackage_ota.zip"%project):
        print "=========WARNING: not exist KK2L_multipackage_ota.zip&L2L_multipackage_ota.zip in out dir =============="
        wt_sys("cp android/out/target/product/%s/%s-ota-*.zip %s/otafull/update.zip"%(project,project,ver_inn))
    os.chdir(root)
    wt_print("m","Generate Lenovo format ...")
    repdir = "%s\(%s\)"%(ver_cus,ver_inn)
    wt_remkd(repdir)   
    wt_sys("mkdir -p %s/doc"%repdir)
    wt_sys("mkdir -p %s/ota_update"%repdir)
    wt_sys("mkdir -p %s/%s"%(repdir,ver_cus))
    wt_sys("mkdir -p %s/%s/%s"%(repdir,ver_cus,ver_cus))
    MD51 = ""
    MD52 = ""
    MD53 = ""
    MD54 = ""

    if os.path.exists("android/wingcust/%s/%s/version/wt_doc"%(ds1["project"],custdir)):
        wt_sys("cp android/wingcust/%s/%s/version/wt_doc/* %s/doc/. -af"%(ds1["project"],custdir,repdir))
    if os.path.exists("%s/otadiff"%ver_inn):
        wt_sys("cp %s/otadiff/* %s/ota_update/."%(ver_inn,repdir))
    if os.path.exists("%s/doc/Release_notes.xls"%ver_inn):
        wt_sys("cp %s/doc/Release_notes.xls %s/%s/."%(ver_inn,repdir,ver_cus))
    if os.path.exists("android/wingcust/%s/%s/version/tool"%(ds1["project"],custdir)):
        wt_sys("cp android/wingcust/%s/%s/version/tool %s/%s/%s/. -af"%(ds1["project"],custdir,repdir,ver_cus,ver_cus))
        wt_sys("cd %s/%s/%s;zip -r -m downloadtool.zip tool;cd -"%(repdir,ver_cus,ver_cus))
    if os.path.exists("%s/otafull/update.zip"%ver_inn):
        wt_sys("cp %s/otafull/update.zip %s/%s/."%(ver_inn,repdir,ver_cus))
        wt_sys("cd %s/%s;zip -r -m %s_otafull.zip update.zip;cd -"%(repdir,ver_cus,ver_cus))
        MD52 = wt_get("md5sum %s/%s/%s_otafull.zip"%(repdir,ver_cus,ver_cus))
        try:
            MD52 = MD52.split()[0]
        except Exception:
            MD52 = ""
    if os.path.exists("%s/otafull/L2L_multipackage_ota.zip"%ver_inn):
        wt_sys("cp %s/otafull/L2L_multipackage_ota.zip %s/%s/."%(ver_inn,repdir,ver_cus))
        wt_sys("cd %s/%s;zip -r -m %s_otafull_L2L.zip L2L_multipackage_ota.zip;cd -"%(repdir,ver_cus,ver_cus))
        MD53 = wt_get("md5sum %s/%s/%s_otafull_L2L.zip"%(repdir,ver_cus,ver_cus))
        try:
            MD53 = MD53.split()[0]
        except Exception:
            MD53 = ""
    if os.path.exists("%s/otafull/KK2L_multipackage_ota.zip"%ver_inn):
        wt_sys("cp %s/otafull/KK2L_multipackage_ota.zip %s/%s/."%(ver_inn,repdir,ver_cus))
        wt_sys("cd %s/%s;zip -r -m %s_otafull_KK2L.zip KK2L_multipackage_ota.zip;cd -"%(repdir,ver_cus,ver_cus))
        MD54 = wt_get("md5sum %s/%s/%s_otafull_KK2L.zip"%(repdir,ver_cus,ver_cus))
        try:
            MD54 = MD54.split()[0]
        except Exception:
            MD54 = ""
    if os.path.exists("%s/software"%ver_inn):
        wt_sys("cp %s/software/*LRX22G* %s/%s/%s/%s_VERSION.zip"%(ver_inn,repdir,ver_cus,ver_cus,ver_cus))
        MD51 = wt_get("md5sum %s/%s/%s/%s_VERSION.zip"%(repdir,ver_cus,ver_cus,ver_cus))
        try:
            MD51 = MD51.split()[0]
        except Exception:
            MD51 = ""
    if not MD51 == "":
        os.system("echo 'USB MD5:%s' >> %s/%s/MD5.txt"%(MD51,repdir,ver_cus))
    if not MD52 == "":
        os.system("echo 'SD  MD5:%s' >> %s/%s/MD5.txt"%(MD52,repdir,ver_cus))
    if not MD53 == "":
        os.system("echo 'SD  MD5:%s' >> %s/%s/MD5.txt"%(MD53,repdir,ver_cus))
    if not MD54 == "":
        os.system("echo 'SD  MD5:%s' >> %s/%s/MD5.txt"%(MD54,repdir,ver_cus))
    wt_sys("cd %s/%s;zip -r -m %s.zip %s;cd -"%(repdir,ver_cus,ver_cus,ver_cus))
    wt_successed()

##to version system upload	by luz	
def uptosystempackage():
    os.chdir(root)
    wt_print("m","uptosystempackage...")
    ver_cus=gen_cus_version()
    cus_inn=ver_cus+"\("+ver_inn+"\)"
    cus_inn_ota=cus_inn+"_ota"
    wt_print("m","Begin to uptosysten package files...")
    wt_sys("mkdir %s/%s"%(ver_inn,cus_inn))
    wt_sys("mkdir %s/%s"%(ver_inn,cus_inn_ota))
    wt_sys("mkdir %s/%s/doc"%(ver_inn,cus_inn))
    wt_sys("mkdir %s/%s/software"%(ver_inn,cus_inn))
    wt_sys("cp -r  %s/otadiff %s/otafull %s/%s/"%(ver_inn,ver_inn,ver_inn,cus_inn_ota))
    wt_sys("rm -rf %s/%s/otafull/base.zip"%(ver_inn,cus_inn_ota))
    wt_sys("cd  %s/%s/ ;zip -r %s.zip otafull otadiff ;cd -"%(ver_inn,cus_inn_ota,cus_inn_ota))
    wt_sys("unzip  %s/software/*.zip -d %s/%s/software/"%(ver_inn,ver_inn,cus_inn))
def repackage_wingtech():
    os.chdir(root)
    uptosystempackage()

def repackage():
    os.chdir(root)
    if ds1["lenovo"] == True or project.startswith("wt82918tra"):
        repackage_lenovo()
    else:
        repackage_wingtech()

def upload():
    os.chdir(root)
    wt_print("m","FTP server IP  :%s"%OTA_SERV)
    wt_print("m","FTP server port:%s"%OTA_PORT)
    wt_print("m","FTP server user:%s"%OTA_USER)
    wt_print("m","FTP server pass:%s"%OTA_PASS)
    wt_print("m","Check environment")
    listr = wt_ftp("list","PUBLISH/%s_ota"%ver_inn)
    if "PUBLISH/%s_ota/old.zip"%ver_inn in listr:
        wt_print("e", "old.zip found in FTP: PUBLISH/%s_ota, plz remove it and rerun the script!"%ver_inn)
    base_zip=ver_inn + "/otafull/base.zip"
    if not os.path.exists(base_zip):
        base_zip=ver_inn + "/otafull/update.zip"
    if os.path.exists(base_zip):
        wt_print("m","UPLOAD:base ota package")
        ldir = "PUBLISH/%s_ota"%ver_inn
        rdir = ldir
        wt_mkdir(ldir)
        wt_sys("cp %s %s/old.zip"%(base_zip,ldir))
        current_version_file="android/current_version.txt"
        if os.path.isfile(current_version_file):
            upload_version_file="android/old_version.txt"
            os.system("cp %s %s"%(current_version_file,upload_version_file))
            os.system("cp %s %s"%(upload_version_file,ldir))
        wt_ftp("upload","%s:%s"%(ldir,rdir))
        wt_successed()
    if os.path.exists("%s/debug"%ver_inn):
        wt_print("m","UPLOAD:symbol and vmlinux")
        ldir = "%s/debug"%ver_inn
        rdir = "PUBLISH/%s_debug"%ver_inn
        wt_ftp("upload","%s:%s"%(ldir,rdir))
        wt_successed()
def sign_server_ap_check():
    os.chdir(root)
    filelist=[
    'android/out/target/product/%s/emmc_appsboot.mbn'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.mbn'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.mdt'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.b00'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.b01'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.b02'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.b03'%project,
    'android/out/target/product/%s/system/etc/firmware/venus.b04'%project,
    ]
    for file in filelist:
        if not os.path.isfile(file):
            print "error : not exist file : %s"%file
            sys.exit(-1)

def mk_version_txt():
    os.chdir(root)
    ver_inn = gen_inn_version()
    ver_cus = gen_cus_version()
    cus_inn=ver_cus+"("+ver_inn+")"
    f=file("version.txt","w+")
    f.writelines(cus_inn)
    f.close()
    os.system("mv version.txt android/out/target/product/%s/fullbuild"%project)
#main function
if __name__ == '__main__':
    root = wt_get("pwd")
    parse_options(sys.argv[1:],ds1)
    custdir = "base" if ds1["customer"] == "" else ds1["customer"]
    OTA_SERV = gen_ota_server()
    OTA_PORT = gen_ota_port()
    OTA_USER = gen_ota_user()
    OTA_PASS = gen_ota_pass()
    project = gen_project()
    ver_inn = gen_inn_version()
    ver_tcl_custom_svn_version = gen_tcl_custom_svn_version()
    ro_tct_sys_ver = gen_ro_tct_sys_ver()

    
    if len(ver_inn) > 50:
    	print "Error: "
    	print "      Internal version number > 50 strings,compile exit."
    	print "      Please check whether the internal version number specification."
    	sys.exit()
    
    if len(ver_tcl_custom_svn_version.strip()) > 0:
        os.system("echo WT_TCL_CUSTOMER_SVN=%s > android/current_version.txt"%ver_tcl_custom_svn_version)
        os.system("echo ro_tct_sys_ver=%s >> android/current_version.txt"%ro_tct_sys_ver)
    ver_cus = gen_cus_version()
    mode = gen_mode()
    check_env()
    new_fingerprint=""

    if ds1["make"] == True:
        make()
    if ds1["server_sign"]=='true':
        sign_server_ap_check()
    if ds1["split"] == True:
        splitandrepack()
    if ds1["package"] == True:
        package()
    if ds1["upload"] == True:
        upload()
    if ds1["record"] == True:
        record()
    if ds1["package"] == True:
        repackage()

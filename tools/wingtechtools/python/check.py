#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import os
import sys
import re
import subprocess


def _check_backslash_blank(file):
	lines=[]
	f=open(file,"r")
	for i in f:
		result=re.search(r"\\ +\r\n",i)
		if result:
			lines.append(i)
			#print i.strip('\n').strip('\r')
		result=re.search(r"\\ +\n",i)
		if result:
			lines.append(i)
			#print i.strip('\n').strip('\r')
	f.close()
	if len(lines):
		print "\033[31mwarning:%s contions \"\ \" line\n\033[0m %s "%(file,lines)

def check_backslash_blank(file):
	f=open(file,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		if os.path.isfile(src):
			if not ( src.endswith('.zip') or src.endswith('.bin') or src.endswith('.rar') ):
				_check_backslash_blank(src)
		else:
			print "not exist %s"%src
	f.close()

def check_xml_format(overlay_topdir,file,overlay_path):
	file_temp=[]
	overlay_path_temp=[]
	f=open(file,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		if src.endswith("xml"):
			file_temp.append(src)
	f.close()
	if len(file_temp)==0:
		#print "not exist xml file"
		return
	i=0
	while i < len(file_temp):
		file_temp[i]=file_temp[i].replace(overlay_topdir,'')
		i=i+1
	i=0
	while i < len(overlay_path):
		overlay_path_temp.append(overlay_path[i].replace(overlay_topdir,''))
		i=i+1
	#print file_temp
	i=0
	while i < len(file_temp) :
		#print i
		j=0
		while j < len(file_temp) :
			#print "---%s"%j
			if not i==j :
				result=re.search(file_temp[i],file_temp[j])
				if result:
					#print "_____%s___%s_____"%(file_temp[i],file_temp[j])
					del file_temp[i]
					del file_temp[j-1]
					i=i-1
					break
			j=j+1
		i=i+1
	#至此 file_temp 只修改了一次
	#print file_temp
	if len(file_temp)==0:
		print "find xml changed and both modified"
		return
	i=0
	while i < len(file_temp):
		j=0
		while j<len(overlay_path_temp):
			result=re.search(overlay_path_temp[j],file_temp[i])
			if result:
				#print "warning:%s is changed only in path %s"%(file_temp[i],overlay_path_temp[j])
				del file_temp[i]
				i=i-1
				break
			j=j+1
		i=i+1
	if len(file_temp)==0:
		return
	i=0
	while i < len(file_temp):
		j=0
		while j<len(overlay_path_temp):
			cmd="find "+overlay_topdir + overlay_path_temp[j] + " -name `basename " + file_temp[i] + "` | grep " + file_temp[i]
			result=runCMD(cmd)
			if len(result) > 0:
				print "\033[31mwarning:%s changed but not modified in %s file\033[0m"%(file_temp[i],result)
			#else:
				#print "warning:%s is changed and not find the same file in system path %s"%(file_temp[i],overlay_path_temp[j])
			j=j+1
		i=i+1

def runCMD(cmd):
	res=subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,close_fds=True)
	result=res.stdout.readlines()
	return result

# 不是unix 返回名字
def check_not_unix_format(filename):
	f=open(filename,"r")
	for i in f:
		result=re.search(r"\r\n",i)
		if result:
			f.close()
			return filename
		result=re.search(r"\r",i)
		if result:
			f.close()
			return filename
	f.close()
	return False
	
def check_unix_format_from_filelist(filetype,filelist):
	filelists=[]
	f=open(filelist,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		if src.endswith(filetype):
			if os.path.isfile(src):
				result=check_not_unix_format(src)
				if result:
					filelists.append(result)
			else:
				print "%s not exist"%src
				
	f.close()
	if len(filelists):
		i=0
		while i < len(filelists):
			print "\033[31mwarning:%s is not linux file\033[0m"%filelists[i]
			i = i+1

def check_type_in_path_from_filelist(filetype,path,filelist):
	if filetype == "NONE":
		cmd="grep \"" + path + "\" " + filelist
	else :
		cmd="grep \"" + path + "\" " + filelist  +" | " + "grep \"\." + filetype + "\$\" " 
	result=runCMD(cmd)
	if len(result) > 0:
		print "\033[31mwarning:%s contions .%s file\n \033[0m %s "%(path,filetype,result)


def check_no_java_file_in_path(path):
	if not os.path.isdir(path):
		print "\033[31mwarning:%s not exist\033[0m"%path
	else:
		cmd="find " + path + " -name \"*.java\""
		result=runCMD(cmd)
		if len(result) > 0:
			print "\033[31mwarning:%s contions .java file\n \033[0m %s "%(path,result)

def check_no_c_file_in_path(path):
	if not os.path.isdir(path):
		print "\033[31mwarning:%s not exist\033[0m"%path
	else:
		cmd="find " + path + " -name \"*.c\""
		result=runCMD(cmd)
		if len(result) > 0:
			print "\033[31mwarning:%s contions .c file\n \033[0m %s "%(path,result)

def java_class_definded_in_file(file,current_file_list):
	CLASSES=[]
	hide_flag=0
	doc_flag=0
	count=0
	f=open(file,"r")
	for i in f:
		src=i.strip('\n').strip('\r')
		result=re.search("/\*",src)
		if result:
			doc_flag=1
		result=re.search("\*/",src)
		if result:
			doc_flag=0
		result=re.search("@hide",src)
		if result and hide_flag==0:
			hide_flag=1
			count=0
			continue
		if doc_flag==1:
			continue
		if hide_flag==1 and count==0:
			result=re.search("public",src)
			if result:
				result1=re.search(";",src)
				if result1:
					hide_flag=0
					count=0
					continue
		if hide_flag==1:
			result=re.search("{",src)
			if  result:
				count=count+1
			result=re.search("}",src)
			if  result:
				if count==1:
					hide_flag=0
				count=count-1
		if hide_flag > 0 or count > 0:
			continue
		result=re.search("public",src)
		if result:
			CLASSES.append(src)
	f.close()
	
	NOT_DEFINED_CLASSES=[]
	i=0
	while i < len(CLASSES):
		#默认找不到
		find_result=1
		j=0
		while j < len(current_file_list):
			j_tmp=CLASSES[i].replace('"','\\"').replace('{','').replace(',',' ').replace(';','')
			list_re=re.compile('\(.*\)|\(.*|\<.*\>|\<.*|\[.*\]|\[.*')
			list_tmp=list_re.sub('',j_tmp)
			list=list_tmp.split()
			if len(list)==0:
				j=j+1
				continue
			cmd="grep -w \"" + list[0] + "\" "+current_file_list[j]
			k=1
			while k<len(list):
				cmd=cmd + " | grep -w \"" +list[k] + "\""
				k+=1
			result=runCMD(cmd)
			if len(result) > 0:
				#有一个找到就算找到
				find_result=0
			j=j+1
		if find_result == 1 :
			NOT_DEFINED_CLASSES.append(CLASSES[i])
		i=i+1
	return NOT_DEFINED_CLASSES

def check_java_class_definded_in_files_from_filelist(filelist,current_file_list):
	f=open(filelist,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		if src.endswith("java"):
			if os.path.isfile(src):
				result=java_class_definded_in_file(src,current_file_list)
				if len(result) > 0:
					print "\033[31mwarning:%s file has public define, but not defined in filelist:\n\t%s , \nplease check it \n \033[0m the public defined is %s "%(src,current_file_list,result)
			else:
				print "%s not exist"%src
	f.close()
def find_dir_updir(basepath,dirname):
	find_dir = basepath
	if not os.path.isdir(basepath) or not basepath.endswith('/'):
		print "not exist basepath or basepath must endswith '/' but \nbasepath = %s"%basepath
	while os.path.abspath(find_dir) != '/' and not os.path.exists(find_dir+dirname):
		find_dir=find_dir+"../"
	return os.path.abspath(find_dir)
def local_repo_code_dir():
	basepath=os.getcwd() + "/"
	return find_dir_updir(basepath,".repo")

def _get_file_checklist(dest_file):
	#os.system("rm "+dest_file)
	config_list=[]
	cmd="find -name '*.git'"
	git_list=runCMD(cmd)
	if len(git_list)==0:
		git_list.append(".")
	i=0
	while i < len(git_list):
		cmd="repo status " + git_list[i].strip('\n').strip('\r') + " | grep -v '^project' |grep '-'| cut -f 2"
		repo_status_result=runCMD(cmd)
		if len(repo_status_result) >0 :
			j=0
			while j < len(repo_status_result):
				file_path=git_list[i].strip('\n').strip('\r')+"/../"+repo_status_result[j]
				cmd="find " + os.path.abspath(file_path).strip('\n').strip('\r') +" -type f >> " + dest_file
				runCMD(cmd)
				j=j+1
		i=i+1
def get_file_checklist(dirs,dest_file):
	if os.path.isfile(dest_file):
		os.system("rm "+dest_file)
	os.system("mkdir -p `dirname " + dest_file +("`"))
	current_dir=os.getcwd()
	i=0
	while i< len(dirs) :
		if os.path.isfile(dirs[i]):
			cmd="echo " + dirs[i] + " >> " + dest_file
			runCMD(cmd)
			i=i+1
			continue
		if not os.path.isdir(dirs[i]):
			print "warning not exist dir: %s"%dirs[i]
			i=i+1
			continue
		os.chdir(dirs[i])
		#print "find changes file from dir : %s"%dirs[i]
		_get_file_checklist(dest_file)
		os.chdir(current_dir)
		i=i+1
#sys 参数传递 + dir_config 读取
def get_config_dirs(dir_config):
	result=[]
	i=1
	while i < len(sys.argv):
		if len(sys.argv[i].strip('[').strip(']').strip(',')) > 0:
			result.append(sys.argv[i].strip('[').strip(']').strip(','))
		i=i+1
	if not os.path.isfile(dir_config):
		print "warning default dirs configfile  %s not exist"%dir_config
		return result
	f=open(dir_config,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		result.append(src)
	f.close()
	return result

def merge_dirs(dirs):
	result=[]
	if len(dirs) == 0:
		#print "dirs is none"
		return result
	i=0
	while i< len(dirs) :
		#cmd="echo " + os.path.abspath(dirs[i]) +" >> " + file
		#runCMD(cmd)
		dirs[i]=os.path.abspath(dirs[i])
		i=i+1
	dirs.sort()
	#取出第一个
	result.append(dirs[0])
	i=0
	j=1
	while j < len(dirs) :
		find_result=re.search(dirs[i],dirs[j])
		if find_result:
			j=j+1
			continue
		result.append(dirs[j])
		j=j+1
		i=i+1
	return result
def check_string_xml_named_in_3languages(git_paths):
	WORK_DIR=os.getcwd()
	i=0
	while i < len(git_paths):
		if os.path.isdir(git_paths[i]):
			os.chdir(git_paths[i])
		else:
			print "warning wrong git path dir :%s"%git_paths[i]
			continue
		# -- file   strings.xml
		xml_name_list=[]
		cmd="repo status . | grep '\-\-' | grep res | grep strings"
		new_file_add_list=runCMD(cmd)
		j=0
		while j<len(new_file_add_list):
			cmd="cat " + new_file_add_list[j].strip('\n').strip('\r').replace(' --\t','') + " | grep -o 'name[ ]*=[ ]*\"\S*\"' "
			#print cmd
			result=runCMD(cmd)
			k=0
			while k<len(result):
				xml_name_list.append(result[k])
				k=k+1
			#print result
			j=j+1
		# modefied file 
		cmd = "git diff res/values*/*strings*.xml | grep '+' | grep -o 'name[ ]*=[ ]*\"\S*\"' "
		result=runCMD(cmd)
		#print result
		k=0
		while k < len(result):
			xml_name_list.append(result[k])
			k=k+1
		# modefied file
		cmd = "git diff --cached res/values*/*strings*.xml | grep '+' | grep -o 'name[ ]*=[ ]*\"\S*\"' "
		result=runCMD(cmd)
		#print result
		k=0
		while k < len(result):
			xml_name_list.append(result[k])
			k=k+1
		#merge
		k=0
		while k < len(xml_name_list):
			xml_name_list[k] = xml_name_list[k].strip('\n').replace(' ','').replace('"','').replace('name=','')
			k=k+1
		xml_name_list.sort()
		xml_name_list_merge=list(set(xml_name_list))
		xml_name_list_merge.sort()
		#check xml_name_list_mergedefined 
		k=0
		while k<len(xml_name_list_merge):
			value_paths=['values','values-zh-rCN','values-zh-rTW']
			for path in value_paths :
				cmd="find -name '*.xml' | grep " + path +" | xargs grep " + xml_name_list_merge[k]
				result = runCMD(cmd)
				if not result:
					print "\033[31mwarning: %s should defined in %s ,maybe you should modify %s/%s/strings.xml\033[0m"%(xml_name_list_merge[k],value_paths,git_paths[i],path)
				#else :
					#print "warning: %s find in %s"%(xml_name_list_merge[k],path)
			k=k+1
		i=i+1
	os.chdir(WORK_DIR)
	
def check_word_string(filelist):
	#find xml or java file
	FILES=[]
	f=open(filelist,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		if src.endswith("xml"):
			FILES.append(src)
		#elif src.endswith("java"):
			#FILES.append(src)
	f.close()
	FILES_GIT_PATH=[]
	i=0
	while i < len(FILES):
		#if FILES[i].endswith("strings.xml"):
			#print "\033[31mwarning: strings.xml file '%s' changed please make sure you changed all the strings file \033[0m\n values/strings.xml \n values-zh-rCN/strings.xml \n values-zh-rTW/strings.xml"%FILES[i]
		basepath=os.path.dirname(FILES[i])+"/"
		git_path=find_dir_updir(basepath,".git")
		if not git_path == "/":
			FILES_GIT_PATH.append(git_path)
		#else :
		#	print FILES[i]
		i=i+1
	FILES_GIT_PATH_MERGED = merge_dirs(FILES_GIT_PATH)
	check_string_xml_named_in_3languages(FILES_GIT_PATH_MERGED)

def list_file_will_be_checked(file):
	print "we will check the files :"
	f=open(file,"r")
	for i in f:
		src=i.strip('\n').strip('\r').strip('\t').strip(' ')
		print "\t%s"%src
	f.close()
def usage():
	print "\033[32m    Only check the file before \"git commit\" \033[0m"
	print ""
	print "\033[32m    use CMD: ./wt check {dir1} {dir2} {file1} {file2}\033[0m"
	print "\033[32m    Example:\033[0m"
	print "\033[32m             ./wt check\033[0m"
	print "\033[32m             ./wt check android/packages/apps/Bluetooth/ android/packages/apps/Camera/\033[0m"
	print "\033[32m             ./wt check android/packages/apps/Bluetooth/ android/kernel/drivers/i2c/i2c-core.c\033[0m"
	print ""
	print "\033[32m    default dir config file : android/tools/wingtechtools/python/check_list_dirs \033[0m"

if __name__ == '__main__':
	print "="*100
	usage()
	print "="*100
	LOCAL_WT_CODE_DIR = local_repo_code_dir()
	if LOCAL_WT_CODE_DIR =='/':
		print "not find .repo"
		sys.exit(-1)
	#CONFIG_DIR contains default dirs which need to be checked
	CONFIG_DIR = LOCAL_WT_CODE_DIR + "/android/tools/wingtechtools/python/check_list_dirs"
	CONFIG_LIST_temp=get_config_dirs(CONFIG_DIR)
	if len(CONFIG_LIST_temp)==0:
		print "there is no dirs to check"
		sys.exit(-1)
	#去除重复
	CONFIG_LIST=merge_dirs(CONFIG_LIST_temp)
	print "we will check the dirs: %s"%CONFIG_LIST

	#FILE_TO_CHECK will be generated
	FILE_TO_CHECK = LOCAL_WT_CODE_DIR + "/android/tools/wingtechtools/build/PRE_GEN/file_to_check"
	get_file_checklist(CONFIG_LIST,FILE_TO_CHECK)
	if not os.path.isfile(FILE_TO_CHECK):
		print "there is no file changes , quit"
		sys.exit(-1)
	
	list_file_will_be_checked(FILE_TO_CHECK)
	print "="*100
	#check overlaypath 有修改 同步修改OVERLAY_PATH里面的同名文件
	OVERLAY_PATH=[]
	OVERLAY_PATH.append(LOCAL_WT_CODE_DIR+"/android/device/qcom")
	OVERLAY_PATH.append(LOCAL_WT_CODE_DIR+"/android/wingcust")
	
	OVERLAY_PATH_TOP=LOCAL_WT_CODE_DIR+"/android/"
	
	check_xml_format(OVERLAY_PATH_TOP,FILE_TO_CHECK,OVERLAY_PATH)
	
	#check "\ "
	check_backslash_blank(FILE_TO_CHECK)
	
	#check "*.prop" must be unix type   
	check_unix_format_from_filelist("prop",FILE_TO_CHECK)
	check_unix_format_from_filelist("sh",FILE_TO_CHECK)
	check_unix_format_from_filelist("mk",FILE_TO_CHECK)
	
	# #check java file in path
	# LOCAL_WT_TOP_CODE_DIR = "/home/lixiaoguang/gt/android"
	# CHECK_FILE_PATH1 = LOCAL_WT_TOP_CODE_DIR + "/device/qcom"
	# check_no_java_file_in_path(CHECK_FILE_PATH1)
	# check_no_c_file_in_path(CHECK_FILE_PATH1)
	
	# CHECK_FILE_PATH2 = LOCAL_WT_TOP_CODE_DIR + "/wingcust"
	# check_no_java_file_in_path(CHECK_FILE_PATH2)
	# check_no_c_file_in_path(CHECK_FILE_PATH2)
	
	#check java file in path
	check_type_in_path_from_filelist("java","android/device/qcom",FILE_TO_CHECK)
	check_type_in_path_from_filelist("java","android/wingcust",FILE_TO_CHECK)
	check_type_in_path_from_filelist("c","android/device/qcom",FILE_TO_CHECK)
	check_type_in_path_from_filelist("c","android/wingcust",FILE_TO_CHECK)
	check_type_in_path_from_filelist("java","/res/",FILE_TO_CHECK)
	
	#check class which defied "public ***"
	CURRENT_FILE_LIST=[]
	CURRENT_FILE_LIST.append(LOCAL_WT_CODE_DIR+"/android/frameworks/base/api/current.txt")
	check_java_class_definded_in_files_from_filelist(FILE_TO_CHECK,CURRENT_FILE_LIST)
	
	#check word_string
	check_word_string(FILE_TO_CHECK)
	
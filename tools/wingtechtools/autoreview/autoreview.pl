#!/usr/bin/perl
#author=zwb

use Encode;
#use strict;

#print "autoreview.pl::start...\n";

if(scalar(@ARGV) < 1){
	die "autoreview.pl::parameter error";
}else
{
	$toreview_file=$ARGV[0];
}


open(TMP,"<$toreview_file") or die "autoreview.pl::can't open file $toreview_file!";
my @lines=<TMP>;
close TMP;

my $reviewed =undef;
foreach my $line(@lines){

  if($line =~ /^\s*revision:\s([0-9a-f]+)\s*$/){
  	
   my $commit = $1;
   $reviewed = "true";
   system("ssh -p 29418 192.168.7.174 gerrit review $commit --code-review +2");
   
  }
}

use encoding "utf-8",STDOUT=>"utf-8";
if($reviewed eq undef){
	print "没什么需要审核的节点。\n";
	exit;
}


print "现在请进入gerrit审核服务器确认打钩的状态是否正确y/n\n";
$confirm=<STDIN>;
chomp($confirm);
if($confirm ne "y"){
  exit;
}

print "正在提交代码..\n";
foreach my $line(@lines){

  if($line =~ /^\s*revision:\s([0-9a-f]+)\s*$/){
  	
   my $commit = $1;

    system("ssh -p 29418 192.168.7.174 gerrit review $commit --verified +1");
    system("ssh -p 29418 192.168.7.174 gerrit review $commit --submit --message \"This-is-autoreview\"");
   
  }
}


